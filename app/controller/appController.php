<?php
require_once 'config.php';
require_once 'security.php';

/**
* 
*/
class AppController extends Security
{
	
	public function __construct()
	{
		date_default_timezone_set("Asia/Jakarta");
	}

	public function loginTrainer($login) {
		$email = $this->clean_post($login['email']);
		$pwd   = $this->clean_post($login['password']);
		$pass  = base64_encode(md5($pwd));

		$query = $this->query("SELECT userId, email, fullname FROM user WHERE email = '$email' AND password = '$pass'");
		if(mysqli_num_rows($query) > 0) {
			$error = false;
			$users = $query->fetch_assoc();
			//session data user
			$_SESSION['userId']   = $users['userId'];
			$_SESSION['email']    = $users['email'];
			$_SESSION['fullname'] = $users['fullname'];

			if($users['userId'] == 1) {
				echo "<script>window.location.replace('forest/')</script>";
			}
		}
	}

	public function regitTrainer($regis) {
		$fullname = $this->clean_post($regis['fullname']);
		$email    = $this->clean_post($regis['email']);
		$pass     = $this->clean_post($regis['password']);
		$pwd      = base64_encode(md5($pass));

		// cek user ditabel apakah ada
		$user = $this->query("SELECT email FROM user WHERE email = '$email'");
		if(mysqli_num_rows($user) > 0) {
			echo "<script>alert('Data Sudah ada')</script>";
		} else {
			$query    = $this->query("INSERT INTO user (email, fullname, password) VALUES ('$email', '$fullname', '$pwd')");

			if($query == TRUE) {
				echo "<script>alert('Registrasi anda berhasil')</script>";
			}
		}
	}

	public function curlHttp($url) {
		// inisiasi dengan curl
		$ch = curl_init();

		 // set url 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    
	    // set user agent    
	    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

	    // return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	    // $output contains the output string 
	    $output = curl_exec($ch); 

	    // tutup curl 
	    curl_close($ch);      

	    // mengembalikan hasil curl
	    return $output;
	}

	public function pokemonAdd($poke) {
		$id 	= $this->clean_all($poke['id']);
		$nama	= $this->clean_post($poke['nama']);
		$pjg	= $this->clean_post($poke['panjang']);
		$brt	= $this->clean_post($poke['berat']);
		$img	= $this->clean_post($poke['image']);

		$query  = $this->query("INSERT INTO bag (pokemonId, nama, panjang, berat, image) VALUES ('$id', '$nama', '$pjg', '$brt', '$img')");
		if($query == TRUE) {
			echo "<script>alert('Pokemon berhasil ditangkap') 
            location.replace('../poke/')</script>";
		}
	}

	public function listPokedex() {
		$rows  = array();
		$query = $this->query("SELECT pokemonId, nama, panjang, berat, image, created_at FROM bag ORDER BY created_at DESC");
		while($row  = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}
}

?>