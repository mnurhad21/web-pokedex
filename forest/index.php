<?php
include_once '../app/controller/appController.php';

$log = new AppController();

$base = 'https://pokeapi.co/api/v2/pokemon/';

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../assets/css/forest.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet"/>
    <style type="text/css">
        #pokedex {
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
            grid-gap: 10px;
            padding-inline-start: 0;
        }

        .cards {
            list-style: none;
            padding: 20px;
            background-color: #f4f4f4;
            color: #222;
            text-align: center;
        }

        .cards:hover {
            animation: bounce 0.5s linear;
        }

        .cards-heading {
            text-align: center;
        }

        .cards-title {
            text-transform: capitalize;
            margin-bottom: 0px;
            font-size: 30px;
            font-weight: normal;
        }
        .cards-subtitle {
            margin-top: 5px;
            color: #666;
            font-weight: lighter;
        }

        .cards-image {
            height: 170px;
        }
    </style>
    <title>Pokemon</title>
</head>
<body>
    <header class="header">
        <div class="circle c1"></div>
        <div class="circle c2"></div>
        <h1 class="logo">
           <div class="arrow"></div>
        </h1>
        <h2 class="slogan">DAFTAR POKEMON GO<span class="ps"></span></h2>
    </header>
    <div class="container">
        <ul id="pokedex">
        <?php
        // simpan pokemon
        if(isset($_POST['submit'])) {
            $poke['id']      = $_POST['id'];
            $poke['image']   = $_POST['image'];
            $poke['nama']    = $_POST['nama'];
            $poke['panjang'] = $_POST['panjang'];
            $poke['berat']   = $_POST['berat'];
            $tangkapPokemon  = $log->pokemonAdd($poke);
        }
        // looping pookemon
        for($id = 1; $id <= 30; $id++) {
            $data = file_get_contents($base.$id.'/');
            $pokemon = json_decode($data); 
            ?> 
                <li class="cards">
                    <form action="" method="post">
                        <h3 class="cards-heading"><?= $pokemon->id; ?></h3>
                        <img class="cards-image" src="<?= $pokemon->sprites->front_default; ?>"/>
                        <h2 class="cards-title"><?= $pokemon->name; ?></h2>
                        <p class="cards-subtitle">Panjang : <?= $pokemon->height; ?></p>
                        <p class="cards-subtitle">Berat : <?= $pokemon->weight; ?></p>
                        <div style="font-size:1.5em">
                            <input type="hidden" name="id" value="<?= $pokemon->id; ?>">
                            <input type="hidden" name="image" value="<?= $pokemon->sprites->front_default; ?>">
                            <input type="hidden" name="nama" value="<?= $pokemon->name; ?>">
                            <input type="hidden" name="panjang" value="<?= $pokemon->height; ?>">
                            <input type="hidden" name="berat" value="<?= $pokemon->weight; ?>">
                           <button type="submit" name="submit" class="button6" style="background-color:#42cc8c;">Tangkap</button>
                        </div>
                    </form>
                </li>
      <?php } ?>
      </ul>
    </div>
</body>
</html>