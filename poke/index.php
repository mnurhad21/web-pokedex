<?php
include_once '../app/controller/appController.php';

$log   = new AppController();

$pokes = $log->listPokedex();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pokedex</title>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="../assets/css/poke.css">
    <style type="text/css">
    	.container {
		    padding: 40px;
		    margin: 0 auto;
		}

        h1 ,h2, h3 {
		    text-transform: uppercase;
		    text-align: center;
		    font-size: 35px;
		}

    	#pokedex {
            display: grid;
            grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
            grid-gap: 10px;
            padding-inline-start: 0;
        }

        .card {
            list-style: none;
            padding: 20px;
            background-color: #f4f4f4;
            color: #222;
            text-align: center;
        }

        .card:hover {
            animation: bounce 0.5s linear;
        }

        .card-heading {
            text-align: center;
        }

        .card-title {
            text-transform: capitalize;
            margin-bottom: 0px;
            font-size: 30px;
            font-weight: normal;
        }
        .card-subtitle {
            margin-top: 5px;
            color: #666;
            font-weight: lighter;
        }

        .card-image {
            height: 170px;
        }
    </style>
	<title>List Pokemon By Trainer</title>
</head>
<body>
  <header class="main-header">
    <div class="header-wrapper">
      <div class="main-logo">Pokedex</div>
      <nav>
        <ul class="main-menu">
          <li><a href="../forest">Pokemon</a></li>
          <li><a href="#section-5">Pokemon By Trainer</a></li>
        </ul>
      </nav>
    </div>
  </header>
  <section id="section-1">
    <div class="content-slider">
      <input type="radio" id="banner1" class="sec-1-input" name="banner" checked>
      <input type="radio" id="banner2" class="sec-1-input" name="banner">
      <input type="radio" id="banner3" class="sec-1-input" name="banner">
      <input type="radio" id="banner4" class="sec-1-input" name="banner">
      <div class="slider">
        <div id="top-banner-1" class="banner">
          <div class="banner-inner-wrapper">
            <h2>Creative</h2>
            <h1>Welcome<br>to Pokedex</h1>
            <div class="line"></div>
            <div class="learn-more-button"><a href="#section-2">Learn More</a></div>
          </div>
        </div>
        <div id="top-banner-2" class="banner">
          <div class="banner-inner-wrapper">
            <h2>What We Do</h2>
            <h1>Great<br>Pokedex</h1>
            <div class="line"></div>
            <div class="learn-more-button"><a href="#section-4">Learn More</a></div>
          </div>
        </div>
        <div id="top-banner-3" class="banner">
          <div class="banner-inner-wrapper">
            <h2>Here We Are</h2>
            <h1>We Are<br>Pokedex</h1>
            <div class="line"></div>
            <div class="learn-more-button"><a href="#section-6">Learn More</a></div>
          </div>
        </div>
        <div id="top-banner-4" class="banner">
          <div class="banner-inner-wrapper">
            <h2>Our Contacts</h2>
            <h1>Welcome<br>to Pokedex</h1>
            <div class="line"></div>
            <div class="learn-more-button"><a href="#main-footer">Learn More</a></div>
          </div>
        </div>
      </div>
      <nav>
        <!-- <div class="controls">
          <label for="banner1"><span class="progressbar"><span class="progressbar-fill"></span></span><span>01</span> Intro</label>
          <label for="banner2"><span class="progressbar"><span class="progressbar-fill"></span></span><span>02</span> Work</label>
          <label for="banner3"><span class="progressbar"><span class="progressbar-fill"></span></span><span>03</span> About</label>
          <label for="banner4"><span class="progressbar"><span class="progressbar-fill"></span></span><span>04</span> Contacts</label>
        </div> -->
      </nav>
    </div>
  </section>
  <div class="container">
  	<ul id="pokedex">
  		<?php 
  		 $no = 1;
  		 foreach($pokes as $poke) :
  		 ?>
  		<li class="card">
  			<h3 class="card-heading"><?= $no++; ?></h3>
            <img class="card-image" src="<?= $poke['image']; ?>"/>
            <h2 class="card-title"><?= $poke['nama']; ?></h2>
            <p class="card-subtitle">Panjang : <?= $poke['panjang']; ?></p>
            <p class="card-subtitle">Berat : <?= $poke['berat']; ?></p>
            <p class="card-subtitle">Ditangkap : <?= date('d M Y H:i', strtotime($poke['created_at'])); ?></p>
  		</li>
  		<?php endforeach; ?>
  	</ul>
  </div>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/poke.js"></script>
</body>
</html>