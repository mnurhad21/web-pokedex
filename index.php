<?php 
include_once 'app/controller/appController.php';

$log = new AppController();

if(isset($_POST['logIn'])) :
  $login['email']    = $_POST['email'];
  $login['password'] = $_POST['password'];
  $loginData = $log->loginTrainer($login);
elseif(isset($_POST['regist'])) :
  $regis['fullname'] = $_POST['fullname'];
  $regis['email']    = $_POST['email'];
  $regis['password'] = $_POST['password'];
  $registUser = $log->regitTrainer($regis);
endif;
?>
<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/login.css">
	<title>Login / Register Pokedex</title>
  <link rel="shortcut icon" href="https://koomik.id/assets/Icon/favicon.png" type="image/x-icon">
</head>
<body>
<section class="user">
  <div class="user_options-container">
    <div class="user_options-text">
      <div class="user_options-unregistered">
        <h2 class="user_unregistered-title">Don't have an account?</h2>
        <p class="user_unregistered-text">Start looking at the future with Koomik.id</p>
        <button class="user_unregistered-signup" id="signup-button">Sign up</button>
      </div>

      <div class="user_options-registered">
        <h2 class="user_registered-title">Have an account?</h2>
        <p class="user_registered-text">Start looking at the future with Koomik.id</p>
        <button class="user_registered-login" id="login-button">Login</button>
      </div>
    </div>
    
    <div class="user_options-forms" id="user_options-forms">
      <div class="user_forms-login">
        <h2 class="forms_title">Login</h2>
        <form class="forms_form" method="post">
          <fieldset class="forms_fieldset">
            <div class="forms_field">
              <input type="email" placeholder="Email" class="forms_field-input" name="email" required autofocus />
            </div>
            <div class="forms_field">
              <input type="password" placeholder="Password" class="forms_field-input" name="password" required />
            </div>
          </fieldset>
          <div class="forms_buttons">
            <button type="button" class="forms_buttons-forgot">Forgot password?</button>
            <input type="submit" name="logIn" value="Log In" class="forms_buttons-action">
          </div>
        </form>
      </div>
      <div class="user_forms-signup">
        <h2 class="forms_title">Sign Up</h2>
        <form class="forms_form" method="post">
          <fieldset class="forms_fieldset">
            <div class="forms_field">
              <input type="text" placeholder="Full Name" class="forms_field-input" name="fullname" required />
            </div>
            <div class="forms_field">
              <input type="email" name="email" placeholder="Email" class="forms_field-input" required />
            </div>
            <div class="forms_field">
              <input type="password" name="password" placeholder="Password" class="forms_field-input" required />
            </div>
          </fieldset>
          <div class="forms_buttons">
            <input type="submit" name="regist" value="Sign up" class="forms_buttons-action">
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript" src="assets/js/login.js"></script>
</body>
</html>